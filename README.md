# [Janus Janus WebRTC Server](https://github.com/meetecho/janus-gateway/) Ubuntu backports

Backport of latest janus version in ubuntu repo:

* Ubuntu Bionic Beaver 18.04
  * `0.7.3-2` - [Janus changelog](https://github.com/meetecho/janus-gateway/blob/master/CHANGELOG.md#v073---2019-07-10) - [Ubuntu changelog](http://changelogs.ubuntu.com/changelogs/pool/universe/j/janus/janus_0.7.3-2/changelog)
* Ubuntu Focal Fossa 20.04
  * `1.1.2-1build5` [Ubuntu changelog](https://changelogs.ubuntu.com/changelogs/pool/universe/j/janus/janus_1.1.2-1build5/changelog)
* Ubuntu Jammy Jellyfish 22.04
  * `1.1.2-1build5` [Ubuntu changelog](https://changelogs.ubuntu.com/changelogs/pool/universe/j/janus/janus_1.1.2-1build5/changelog)

## Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-janus.asc https://packaging.gitlab.io/janus/gpg.key
```

## Add repo to apt

```bash
. /etc/lsb-release; echo "deb https://packaging.gitlab.io/janus/$DISTRIB_CODENAME $DISTRIB_CODENAME main" | sudo tee /etc/apt/sources.list.d/morph027-janus.list
```

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).
