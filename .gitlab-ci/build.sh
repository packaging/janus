#!/bin/bash

set -e

if [[ "${DEBUG}" ]]; then
    set -x
fi

export DEBIAN_FRONTEND=noninteractive

. /etc/lsb-release

ARCH="${ARCH:-amd64}"
WORK_DIR="$(mktemp -d)"
cd "${WORK_DIR}"

case "${DISTRIB_CODENAME}" in
"bionic")
    janus="0.7.3-2build1"
    ;;
"focal")
    janus="1.1.2-1build5"
    ;;
"jammy")
    janus="1.1.2-1build5"
    ;;
esac

mkdir -p "${CI_PROJECT_DIR}/${CI_JOB_NAME}"

prepare() {
    printf "\e[0;36mInstalling toolchain ...\e[0m\n"
    apt-get update
    apt-get -y install \
        curl \
        ccache

    build_packages=()
    mkdir -p build-deps
    curl -sL https://deb.nodesource.com/gpgkey/nodesource.gpg.key >/etc/apt/trusted.gpg.d/nodesource.asc
    case "${DISTRIB_CODENAME}" in
    "bionic")
        node="10"
        ;;
    *)
        node="18"
        ;;
    esac
    echo "deb https://deb.nodesource.com/node_${node}.x ${DISTRIB_CODENAME} main" >/etc/apt/sources.list.d/nodesource.list
    apt-get update
    apt-get -y install nodejs
    npm install -g acorn rollup@0.50.0 terser
    test -f /usr/bin/uglifyjs || ln -s /usr/lib/node_modules/terser/bin/uglifyjs /usr/bin/
    test -f /usr/bin/uglifyjs.terser || ln -s /usr/lib/node_modules/terser/bin/uglifyjs /usr/bin/uglifyjs.terser
    if [ "${DISTRIB_CODENAME}" == "bionic" ]; then
        curl -sL https://apt.kitware.com/keys/kitware-archive-latest.asc >/etc/apt/trusted.gpg.d/kitware.asc
        echo "deb https://apt.kitware.com/ubuntu/ ${DISTRIB_CODENAME} main" >/etc/apt/sources.list.d/kitware.list
        echo "deb http://de.archive.ubuntu.com/ubuntu bionic-backports main" >/etc/apt/sources.list.d/bionic-backports.list
        apt-get update
        apt-get -y -t bionic-backports install debhelper
        build_packages+=("https://github.com/jgm/pandoc/releases/download/2.17.0.1/pandoc-2.17.0.1-1-${ARCH}.deb")
        build_packages+=('http://archive.ubuntu.com/ubuntu/pool/universe/libj/libjs-webrtc-adapter/libjs-webrtc-adapter_7.5.1~ds-1_all.deb')
        build_packages+=('http://archive.ubuntu.com/ubuntu/pool/universe/libj/libjs-bootbox/libjs-bootbox_5.3.2~dfsg-1_all.deb')
    fi
    for package in "${build_packages[@]}"; do
        if [ ! -f "$package" ]; then
            (
                cd build-deps
                curl -sLO "$package"
            )
            apt-get -y install "./build-deps/${package##*/}"
        fi
    done
    apt-get -y install cmake devscripts equivs
}

patches() {
    local package="$1"
    local dir="$2"
    cd "${dir}"
    case "${DISTRIB_CODENAME}" in
    "bionic" | "focal" | "jammy")
        case "$package" in
        "janus")
            sed -i 's,cd npm \&\& $(ROLLUP),cd npm \&\& npm install \&\& $(ROLLUP),' debian/rules                 # 1.x
            sed -i 's,cd npm \&\& rollup,cd npm \&\& npm install rollup-plugin-replace \&\& rollup,' debian/rules # 0.x
            sed -i 's,--output\.format umd \\,--output.format umd --output.name Janus \\,' debian/rules
            sed -i '/node-rollup-plugin-replace/d' debian/control
            sed -i '/rollup/d' debian/control
            sed -i '/terser/d' debian/control
            echo "!!! patching debhelper !!!"
            sed -i 's,debhelper-compat (= 13),debhelper-compat (= 12),' debian/control
            EDITOR=/bin/true dpkg-source -q --commit . build-fix
            ;;
        "paho.mqtt.c" | "d-shlibs")
            echo "!!! patching debhelper !!!"
            sed -i 's,debhelper-compat (= 13),debhelper-compat (= 12),' debian/control
            EDITOR=/bin/true dpkg-source -q --commit . debhelper-fix
            rm -f debian/paho.mqtt.c-examples.install
            ;;
        esac
        ;;
    esac
    cd -
}

build() {
    export PATH="/usr/lib/ccache/bin/:$PATH"
    export CCACHE_DIR="$CI_PROJECT_DIR/ccache"
    mkdir -p "$CCACHE_DIR"
    local package
    package="$1"
    package_name="${package##*/}"
    if [ ! -f "${package##*/}" ]; then
        printf "\e[0;36mDownloading %s ...\e[0m\n" "${package##*/}"
        curl -sLO "${package}"
        if [ "${package##*.}" == "dsc" ]; then
            printf "\e[0;36mBuilding %s ...\e[0m\n" "${package##*/}"
            dpkg-source -x "${package##*/}" 2>&1 | tee "${package##*/}".dir
            dir="$(grep 'extracting\s.*\sin\s.*' "${package##*/}".dir | awk '{print $NF}')"
            patches "${package_name%%_*}" "${dir}"
            mk-build-deps "${dir}/debian/control" 2>&1 | tee "${package##*/}".dep
            builddeps="$(grep -o '../.*build-deps.*.deb' "${package##*/}".dep)"
            echo "builddeps = $builddeps"
            apt-get -y install "./${builddeps##*/}" && rm -f "./${builddeps##*/}"
            cd "${dir}" || exit 1
            debuild \
                --preserve-envvar=CCACHE_DIR \
                --prepend-path=/usr/lib/ccache \
                --no-lintian \
                -us -uc \
                -j"$(getconf _NPROCESSORS_ONLN)"
            cd ..
            rm -f janus-demos*deb
            apt-get -y --allow-downgrades install ./*.deb
        fi
    fi
}

packages=('http://archive.ubuntu.com/ubuntu/pool/universe/p/paho.mqtt.c/paho.mqtt.c_1.3.12.orig.tar.gz')
packages+=('http://archive.ubuntu.com/ubuntu/pool/universe/p/paho.mqtt.c/paho.mqtt.c_1.3.12-1.debian.tar.xz')
packages+=('http://archive.ubuntu.com/ubuntu/pool/universe/p/paho.mqtt.c/paho.mqtt.c_1.3.12-1.dsc')

case "${DISTRIB_CODENAME}" in
"bionic" | "focal")
    packages+=('http://archive.ubuntu.com/ubuntu/pool/universe/d/d-shlibs/d-shlibs_0.104.tar.xz')
    packages+=('http://archive.ubuntu.com/ubuntu/pool/universe/d/d-shlibs/d-shlibs_0.104.dsc')
    packages+=('http://archive.ubuntu.com/ubuntu/pool/universe/libs/libsrtp2/libsrtp2_2.4.2-2.debian.tar.xz')
    packages+=('http://archive.ubuntu.com/ubuntu/pool/universe/libs/libsrtp2/libsrtp2_2.4.2.orig.tar.gz')
    packages+=('http://archive.ubuntu.com/ubuntu/pool/universe/libs/libsrtp2/libsrtp2_2.4.2-2.dsc')
    ;;
esac

case "${DISTRIB_CODENAME}" in
"bionic")
    packages+=('http://archive.ubuntu.com/ubuntu/pool/universe/libu/libusrsctp/libusrsctp_0.9.3.0+20190901.orig.tar.xz')
    packages+=('http://archive.ubuntu.com/ubuntu/pool/universe/libu/libusrsctp/libusrsctp_0.9.3.0+20190901-1.debian.tar.xz')
    packages+=('http://archive.ubuntu.com/ubuntu/pool/universe/libu/libusrsctp/libusrsctp_0.9.3.0+20190901-1.dsc')
    packages+=("http://archive.ubuntu.com/ubuntu/pool/universe/j/janus/janus_${janus%%-*}.orig.tar.gz")
    packages+=("http://archive.ubuntu.com/ubuntu/pool/universe/j/janus/janus_${janus}.debian.tar.xz")
    packages+=("http://archive.ubuntu.com/ubuntu/pool/universe/j/janus/janus_${janus}.dsc")
    ;;
"focal" | "jammy")
    packages+=("http://archive.ubuntu.com/ubuntu/pool/universe/j/janus/janus_${janus%%-*}.orig.tar.gz")
    packages+=("http://archive.ubuntu.com/ubuntu/pool/universe/j/janus/janus_${janus}.debian.tar.xz")
    packages+=("http://archive.ubuntu.com/ubuntu/pool/universe/j/janus/janus_${janus}.dsc")
    ;;
esac

prepare

for package in "${packages[@]}"; do
    build "${package}"
done

if [ "${DISTRIB_CODENAME}" == "bionic" ]; then
    cp build-deps/libjs-webrtc-adapter_*.deb build-deps/libjs-bootbox_*.deb "${CI_PROJECT_DIR}/${CI_JOB_NAME}"
fi

rm -f paho.mqtt.c-examples_*_*.deb d-shlibs_*_all.deb
cp -v "${WORK_DIR}"/*.deb "${CI_PROJECT_DIR}/${CI_JOB_NAME}/"
